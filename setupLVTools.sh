#!/bin/bash
# setupLVTool.sh
# ZSS, 2014-08-19
# Script to automatically setup LV Tools for use with git bash
#
# originally from https://github.com/JQIamo/SetList
# modified and updated by Sam Taggart, SAS workshops on 2021-04-19

# Create ~/bin if it doesn't already exist
mkdir -p ~/bin

# Setup LVCompare as the default difftool
cp lvcompareWrapper.sh ~/bin
git config --global --replace-all diff.tool LVCompare
git config --global --replace-all difftool.LVCompare.cmd '~/bin/lvcompareWrapper.sh "$LOCAL" "$REMOTE"'
git config --global --replace-all alias.difflv "difftool --tool=LVCompare"

# Setup the LVMerge command as the default mergetool
cp lvmergeWrapper.sh ~/bin
git config --global --replace-all merge.tool LVMerge
git config --global --replace-all mergetool.LVMerge.cmd '~/bin/lvmergeWrapper.sh "$BASE" "$LOCAL" "$REMOTE" "$MERGED"'
git config --global --replace-all mergetool.LVMerge.trustExitCode false
git config --replace-all alias.mergelv "mergetool --tool=LVMerge"
